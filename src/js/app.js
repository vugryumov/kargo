(function () {
	'use strict';

	angular
		.module("kargoapp", ["ui.router",'angular-loading-bar','ui.bootstrap'])

		.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
			cfpLoadingBarProvider.includeSpinner = true;
		}])

		.config(['$urlRouterProvider','$stateProvider','$locationProvider', routeConfig])


	function routeConfig($urlRouterProvider, $stateProvider, $locationProvider) {

		$urlRouterProvider.otherwise('/posts');

		$locationProvider.html5Mode({
						enabled: true,
						requireBase: false
					});

		$stateProvider

			.state('posts', {
				url: '/posts',
				template: '<postslist posts-data="vm.posts"></postslist>',
				controller: 'PostsCtrl as vm',
				resolve: {
					posts: ['DataService', function(DataService) {
						return DataService.getAll();
					}]
				}
			})

			.state('posts.details', {
				url: '^/{id:int}',
				views : {
					'@' : {
						templateUrl: 'views/details.tpl.html',
						controller: 'DetailsCtrl as vm',
						resolve: {
							post_id: ['$stateParams', function($stateParams) {
								return $stateParams.id;
							}]
						}
					}

				}
			});

	}


}());
