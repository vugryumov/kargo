(function () {

	"use strict";

	angular
		.module('kargoapp')
		.controller('DetailsCtrl',['DataService','post_id', conroller]);

	function conroller(DataService,post_id) {
		var vm = this;
		vm.post_details = {};
		vm.tracking_data = {};
		vm.start_date = new Date('2015-01-01').toUTCString();
		vm.end_date = new Date('2015-03-01').toUTCString();
		vm.min_date = vm.start_date;
		vm.max_date = vm.end_date;
		vm.loadTrackingData = loadTrackingData;

		//datepicker
		vm.status = {
			opened : false
		};
		vm.open = function($event) {
			vm.status.opened = true;
		};

		init(); //asyncronously loads post details and tracking data for a given date range

		function init(){

			DataService
				.getDetails(post_id)
				.then(function response(data){
					vm.post_details = data;
				});

			loadTrackingData();
		}

		function loadTrackingData(){
			 DataService.getTrackers(vm.start_date, vm.end_date)
				 .then(function(databack){
					 console.log(' : databack', databack);
					 vm.tracking_data = databack;
				 });
		};
	}

})();