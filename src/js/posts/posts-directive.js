(function () {

	"use strict";

	angular
		.module('kargoapp')
		.directive('postslist', directive);

	function directive() {
		return {
			restrict: 'EA',
			replace: true,
			require: '^postsData',
			scope: {
				postsData: '='
			},
			templateUrl: 'views/posts.tpl.html'
		}
	}

})();