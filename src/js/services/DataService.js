(function () {

	"use strict";

	angular
		.module('kargoapp')
		.factory('DataService', ['$http', config]);

	function config($http) {
		return {
			getAll : function(){
				return $http
					.get('http://jsonplaceholder.typicode.com/posts')
					.then(function response(response){
						return response.data;
					})
			},

			getDetails : function (id){
				return $http
					.get('http://jsonplaceholder.typicode.com/posts/' + id )
					.then(function onSuccess(response){
						return response.data;
					}, function onError(err){
						console.log(err);
					})
			},

			getTrackers : function(start, end){

				var URL = 'http://kargotest.herokuapp.com/api/trackers';
				var query = '?from=' + start + '&to=' + end;
				var fixed_trackers_data = [];

				return $http
					.get(URL + query)
					.then(function onSuccess(response){

						fixed_trackers_data = fix_missing_dates(response.data.data, start, end);

						return fixed_trackers_data;
					}, function onerror(response){
						console.log('Error getting data from TRackers')
					});


				//This function inserts missing dates into array, dates formatted to ISO
				function fix_missing_dates(arr, start, end){

					var firstDate = new Date(start);
					var lastDate = new Date(end);
					var expected_days = Math.round(Math.abs((firstDate.getTime() - lastDate.getTime())/(24*60*60*1000))) + 1;
					var sorted = arr.sort(function(a,b){
						return new Date(a.date).getTime() - new Date(b.date).getTime();
					});
					var corrected = []; //corrected array with all dates
					var date = firstDate.toISOString();

					for(var i = 0; i < expected_days; i++){
						corrected.push(getDataForDate(date, sorted, i+1));
						date = new Date(date).add({days : 1}).toISOString();
					}

					return corrected;

					function getDataForDate(search_date, sorted, index){
						var date_updated = {
							"id": index,
							"hits": 0,
							"date": search_date
						};

						for(var i = 0, len = sorted.length; i < len; i++) {
							if (new Date(sorted[i].date).toISOString() === search_date) {
								sorted[i].date = new Date(sorted[i].date).toISOString();
								date_updated = sorted[i];
								break;
							}
						}
						return date_updated;
					}}
			}
		}
	}

})();